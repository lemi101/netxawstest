using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.ML.OnnxRuntime;
using Microsoft.ML.OnnxRuntime.Tensors;
using netxawstest.Models;


namespace netxawstest.Controllers
{
    [Route("/score")]
    public class AsesmenController : ControllerBase
    {
        private InferenceSession _session;
        
        public AsesmenController(InferenceSession session)
        {
            _session = session;
        }

        [HttpPost]
        public ActionResult Score(IFormCollection data)
        {
            var fs2 = new DataFS2();
            fs2.Urgensi = float.Parse(data["urgensi"]);
            fs2.LagTime = float.Parse(data["lagTime"]);
            fs2.Kriteria1 = float.Parse(data["kriteria1"]);
            fs2.Kriteria2 = float.Parse(data["kriteria2"]);
            fs2.Kriteria3 = float.Parse(data["kriteria3"]);
            fs2.Kriteria4 = float.Parse(data["kriteria4"]);
            fs2.Kriteria5 = float.Parse(data["kriteria5"]);
            fs2.Kriteria6 = float.Parse(data["kriteria6"]);
            fs2.DesainDatabase = float.Parse(data["desainDatabase"]);
            fs2.ProsesAplikasi = float.Parse(data["prosesAplikasi"]);
            fs2.Antarmuka = float.Parse(data["antarmuka"]);
            fs2.JadwalSDM = float.Parse(data["jadwalSDM"]);

            var result = _session.Run(new List<NamedOnnxValue> 
            { 
                NamedOnnxValue.CreateFromTensor("float_input", fs2.AsTensor()) 
            }).ToList().Last().AsEnumerable<NamedOnnxValue>();

            var inferenceResult = result.First().AsDictionary<string, float>();

            string resKey = inferenceResult.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;

            float res = inferenceResult[resKey] * 100;

            string resultRes = "<html><br><br><br><h1 align='center'>Hasil Rekomendasi Asesmen FS2</h1><br>" + "<h1 align='center'><i>" + resKey.ToString() + "</i></h1><br>" + "<p align='center'><b>Akurasi: " + res.ToString() + "%</b></p>" + "</html>";

            return Content(resultRes, "text/html");
        }
    }
}