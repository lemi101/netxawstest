
using Microsoft.ML.OnnxRuntime.Tensors;

namespace netxawstest.Models
{
    public class DataFS2
    {
        public float Urgensi { get; set; }
        public float LagTime { get; set; }
        public float Kriteria1 { get; set; }
        public float Kriteria2 { get; set; }
        public float Kriteria3 { get; set; }
        public float Kriteria4 { get; set; }
        public float Kriteria5 { get; set; }
        public float Kriteria6 { get; set; }
        public float DesainDatabase { get; set; }
        public float ProsesAplikasi { get; set; }
        public float Antarmuka { get; set; }
        public float JadwalSDM { get; set; }

        public Tensor<float> AsTensor() 
        {
            float[] data = new float[] 
            {
                Urgensi, LagTime, Kriteria1, Kriteria2,
                Kriteria3, Kriteria4, Kriteria5, Kriteria6,
                DesainDatabase, ProsesAplikasi, Antarmuka, JadwalSDM 
            };
            int[] dimensions = new int[] { 1, 12 };
            return new DenseTensor<float>(data, dimensions);
        }
    }
}