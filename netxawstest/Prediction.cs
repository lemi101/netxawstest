namespace netxawstest.Models
{
    public class Prediction
    {
        public float PredictedValue { get; set; }
    }
}